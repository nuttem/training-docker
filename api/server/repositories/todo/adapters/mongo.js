const mongoose = require("mongoose");

var TodoCollection = mongoose.model(
  "Todo",
  new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    text: String,
    isCompleted: Boolean
  })
);

class MongoRepositoryAdaptor {
  constructor(url) {
    try {
      mongoose.connect(url || "mongodb://localhost:27017/test", {
        useNewUrlParser: true
      });

      try {
        TodoCollection.createCollection();
      } catch (error) {
        console.log("error createCollection", error);
      }
    } catch (error) {
      console.log("error connect mongo", error);
    }
  }

  async addTask(text) {
    const result = await TodoCollection.create({ text, isCompleted: false });
    return {
      _id: result._id + "",
      text: result.text + "",
      isCompleted: !!result.isCompleted
    };
  }

  async toggleCompleted(itemId) {
    const result = await TodoCollection.update(
      { _id: mongoose.Types.ObjectId(itemId) },
      { isCompleted: true }
    );
    return result;
  }
  async removeItem(itemId) {
    const result = await TodoCollection.deleteMany({
      _id: mongoose.Types.ObjectId(itemId)
    });
    return result;
  }

  async removeAll() {
    const result = await TodoCollection.deleteMany({});
    return result;
  }
}

module.exports = { MongoRepositoryAdaptor: MongoRepository };
