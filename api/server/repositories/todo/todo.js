const { MongoRepositoryAdaptor } = require("./adapters/mongo");
require("dotenv").config();

const adapter = new MongoRepositoryAdaptor(process.env.MONGO_URL);

class TodoRepo {
  async addTask(text) {
    return adapter.addTask(text);
  }

  async getTask(id) {
    return adapter.addTask(id);
  }

  async getAllTasks() {
    return adapter.getAllTasks();
  }
  async toggleCompleted(id) {
    return adapter.toggleCompleted(id);
  }

  async removeItem(id) {
    return adapter.removeItem(id);
  }
  async removeAll() {
    return adapter.removeAll();
  }
}

module.exports = { TodoRepo };
