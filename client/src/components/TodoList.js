import React from 'react'
import Todo from './Todo'
import TodoForm from './TodoForm'

const TodoList = (props) => {
    return (
        <div>
            {props.todos.map((todo, id) => (
                <Todo todo={todo} key={id} toggleComplete={props.toggleComplete}/>
            ))}
        </div>
    )
}

export default TodoList