import React from 'react'

const TodoForm = (props) => {
    return(
        <form>
            <input name="todo" value={props.value} onChange={props.inputHandleChange} type="text" placeholder="enter a task" />
            <button onClick={props.addTask}>Add</button>
            <button onClick={props.removeItems}>X</button>
        </form>
    )
}

export default TodoForm