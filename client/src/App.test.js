import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { addTodo } from './helper';

describe('Todo App', () => {
  it('should render to show add item', () => {
    const addTask = [
        { id: 1, name: 'one', isComplete: false },        
        { id: 2, name: 'two', isComplete: false }
    ]
    const newTodo = { id: 3, name: 'three', isComplete: false} 
      const expected =[
         { id: 3, name: 'three', isComplete: false },        
         { id: 1, name: 'one', isComplete: false },        
         { id: 2, name: 'two', isComplete: false }    
      ]
      const result = addTodo(addTask, newTodo)
      expect(result).toEqual(expected)
  });
});